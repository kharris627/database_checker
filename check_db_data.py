import pyodbc, requests, threading, json, hmac, sys, atexit, string, psycopg2
from random import randint
from datetime import datetime, date
import os

start_time = datetime.now()

old_hostname = '10.30.177.148'
old_username = 'clearpath'
old_password = ''
old_db = 'lis'

new_hostname = '10.30.177.199'
new_username = 'clearpath'
new_password = ''
new_db = 'lis'

tables = ['patient', 'case', 'specimen_diagnosis']

def start():
	old_conn = psycopg2.connect(host=old_hostname, user=old_username, password=old_password, dbname=old_db)
	new_conn = psycopg2.connect(host=new_hostname, user=new_username, password=new_password, dbname=new_db)
	for table_name in tables:
		print("Table: " + str(table_name))
		columns = get_table_columns(old_conn, table_name)		
		compare_columns(old_conn, new_conn, table_name, columns)
	old_conn.close()
	new_conn.close()

def compare_columns(old_conn, new_conn, table_name, columns):
	old_cur = old_conn.cursor()
	new_cur = new_conn.cursor()
	query = "SELECT " + ", ".join(columns) + ' FROM "' + table_name + '" ORDER BY id asc'
	old_cur.execute(query)
	old_rows = old_cur.fetchall()
	new_cur.execute(query)
	new_rows = new_cur.fetchall()
	same = True
	id_loc = columns.index('id')
	print('id_loc:' + str(id_loc))
	print("old_row count: " + str(len(old_rows)))
	print ('new_row count: ' + str(len(new_rows)))
	for x in range(0, len(old_rows)):
		for y in range(0, len(columns)):			
			if old_rows[x][y] != new_rows[x][y]:
				print(table_name + " id " + str(old_rows[x][id_loc]) + " failed comparison test. " + columns[y] + " column mismatch. Values: " + str(old_rows[x][y]) + " != " + str(new_rows[x][y]))
				same = False
	print("The same? " + str(same))
	
def get_table_columns(conn, table_name):
	cols = []
	cur = conn.cursor()
	query = "SELECT * FROM information_schema.columns WHERE table_name='" + table_name + "'"
	cur.execute(query)
	for row in cur.fetchall():
		cols.append(row[3])
		#print(str(row[3]))
	return cols
	
def get_table_names(conn):
	cur = conn.cursor()
	query = "SELECT table_name FROM information_schema.tables WHERE table_schema='public'"
	cur.execute(query)
	for table_name in cur.fetchall():
		tables.append(table_name[0])

@atexit.register
def end():
	print("Total time: " + str((datetime.now() - start_time)))
	

if __name__ == '__main__':	
	start()